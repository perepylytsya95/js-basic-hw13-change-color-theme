const themeChangeBtn = document.querySelector('.theme_btn');
const changeThemeLink = document.createElement('link');
changeThemeLink.setAttribute('rel', 'stylesheet');
changeThemeLink.setAttribute('href', "./css/style_blue.css");
const green = document.querySelector('.theme_change__green');
const blue = document.querySelector('.theme_change__blue');

const inputFirstName = document.querySelector('.form__input');
const inputLastName = document.querySelector('[placeholder="Last Name"]');


themeChangeBtn.addEventListener('click', function (){
    if (blue.checked){
        document.head.append(changeThemeLink);
        window.localStorage.setItem('theme', 'blue');
        inputFirstName.removeAttribute('value');
        setTimeout(()=>{
            inputFirstName.setAttribute('value', 'Филипп')
        }, 500)
        setTimeout(()=>{
            inputLastName.setAttribute('value', 'Киркоров')
        }, 1000)
    } else if (green.checked){
        changeThemeLink.remove();
        window.localStorage.setItem('theme', 'green');
        inputLastName.removeAttribute('value');
        inputFirstName.removeAttribute('value');
    }
})

window.addEventListener('load', () =>{
    if (localStorage.getItem('theme') === 'blue'){
        document.head.append(changeThemeLink);
        inputFirstName.setAttribute('value', 'Филипп')
        inputLastName.setAttribute('value', 'Киркоров')
    }
    if (localStorage.getItem('theme') === 'green'){
        changeThemeLink.remove();
    }
})

themeChangeBtn.addEventListener('mouseover', function (){
    if(green.checked) {
        themeChangeBtn.style.background = "rgba(1, 129, 63, 1)";
    }
    else if (blue.checked){
        themeChangeBtn.style.background = "blue";
    } else {
        themeChangeBtn.style.background = "white";
    }
})

themeChangeBtn.addEventListener('mouseout', function (){
    themeChangeBtn.style.background = "white";
})

